---
Author: Timea Moder
Version: 109 21-1/CNL 113 862-2, Rev. PA1
Date: 2018-08-02
---
= NAS EPS v15.2.0 Protocol Modules for TTCN-3 Toolset with TITAN, Product Revision Information
:author: Timea Moder
:revnumber: 109 21-1/CNL 113 862-2, Rev. PA1
:revdate: 2018-08-02
:toc:

= Product Revision

== Product

|===
|Old Product number:| 1/CNL 113 862| R1A
|New Product number:| 1/CNL 113 862| R2A
|===

== Included Parts

=== Source Code

[source]
NAS_EPS_Types.ttcn

=== TPD file

[source]
NAS_EPS_v15.2.0_1_CNL113862.tpd

=== Documentation

==== Function Description

[cols="50%,30%,20%",options="header",]
|===
|Document Name |Document Number | Revision
|NAS EPS v15.2.0 Protocol Modules for TTCN-3 Toolset with TITAN,Description |1551-1/CNL 113 862 Uen | A
|===

== Product Dependencies

This product is based on the following products or parts of products:

[cols="50%,50%",options="header",]
|===
|Product Number & Rev. |Product name/File name
|CNL 113 368 R9A or newer |COMMON protocol modules General_Types.ttcn
|===

= Reason for Revision

== Requirement Specification

None.

== Change Requests

[cols="50%,50%",options="header",]
|===
| CR ID| Effect
|===

== Exemption Requests

== Trouble Reports

Trouble reports received until 2018-10-11.

=== Implemented Trouble Reports

[cols="50%,50%",options="header",]
|===
| TR ID| Trouble Effect
|===

=== Not Implemented Trouble Reports

[cols="50%,50%",options="header",]
|===
| TR ID| Trouble Effect
|===

= Product Changes

== R1A

Initial version based upon the OpenALM artifact #1566532.

== R2A

Prepared to be open source. Behavior unchanged.
