---
Author: Timea Moder
Version: 1551-1/CNL 113 862, Rev. PB1
Date: 2018-08-02
---
= NAS EPS v15.2.0 Protocol Modules for TTCN-3 Toolset with TITAN, Function Description
:author: Timea Moder
:revnumber: 1551-1/CNL 113 862, Rev. PB1
:revdate: 2018-08-02
:toc:

*Abstract*

This is the description for the NAS EPS v15.2.0 protocol module. The NAS EPS v15.2.0 protocol module is developed for the TTCN-3 Toolset with Titan. This document should be read together with <<_2,Product Revision Information>>.

= Functionality

The NAS EPS v15.2.0 protocol module implements the message structures of the <<_5, related protocol>> in a formalized way, using the standard specification language, TTCN-3. This allows defining of test data (templates) in the TTCN-3 language and correctly encoding/decoding messages when executing test suites using the Titan TTCN-3 test environment.

The NAS EPS v15.2.0 protocol module uses <<_1,Titan’s RAW encoding attributes>> and hence is usable with the Titan test toolset only.

== Implemented protocols

This set of protocol modules implements protocol messages and constants of the NAS EPS v15.2.0 protocol as described <<_5,here>>, which refers to other 3GPP Technical Specifications described <<_6,here>>, <<_7,here>>, and <<_8,here>>.

=== Modified and non-implemented Protocol Elements

None.

== Backward incompatibilities

None.

== System Requirements

Protocol modules are a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, protocol modules alone do not put specific requirements on the system used. However, in order to compile and execute a TTCN-3 test suite using the set of protocol modules, the following system requirements must be satisfied:

* Titan TTCN-3 Test Executor version CRL 113 200/6 R3A (6.3.pl0) or higher installed. For Installation Guide see <<_4,here>>.

NOTE: This version of the test port is not compatible with Titan releases earlier than CRL 113 200/6 R3A.

= Usage

== Installation

The set of protocol modules can be used in developing TTCN-3 test suites. Since the NAS EPS v15.2.0 protocol is used as a part of a TTCN-3 test suite, this requires TTCN-3 Test Executor to be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor, see the relevant section of the <<_4,Installation Guide for the TITAN TTCN-3 Test Executor>>.

== Configuration

None.

== Examples

None.

= Interface description

== Top Level PDU

The top level PDU is the TTCN-3 record PDU_NAS_EPS.

[[encoding-decoding-and-other-related-functions]]
== Encoding/decoding and other related functions

This product also contains encoding/decoding functions, which assure correct encoding of messages when sent from Titan and correct decoding of messages when received by Titan.

=== Implemented encoding and decoding functions

[width="100%",cols="34%,33%,33%",options="header",]
|====================================================================================
|Name |Type of formal parameters |Type of return value
|`enc_PDU_NAS_EPS` |PDU_NAS_EPS |octetstring
|`dec_PDU_NAS_EPS` |octetstring |PDU_NAS_EPS
|`dec_PDU_NAS_EPS_backtrack` |octetstring, PDU_NAS_EPS |integer
|`enc_EPS_MobileIdentityV_NAS_EPS` |EPS_MobileIdentityV |octetstring
|`dec_EPS_MobileIdentityV_NAS_EPS` |octetstring |EPS_MobileIdentityV
|`dec_EPS_MobileIdentityV_NAS_EPS_backtrack` |octetstring, EPS_MobileIdentityV |integer
|`enc_IMSI_NAS_EPS` |IMSI |octetstring
|`dec_IMSI_NAS_EPS` |octetstring |IMSI
|`dec_IMSI_NAS_EPS_backtrack` |octetstring, IMSI |integer
|`enc_GUTI_NAS_EPS` |GUTI |octetstring
|`dec_GUTI_NAS_EPS` |octetstring |GUTI
|`dec_GUTI_NAS_EPS_backtrack` |octetstring, GUTI |integer
|`enc_IMEI_NAS_EPS` |IMEI |octetstring
|`dec_IMEI_NAS_EPS` |octetstring |IMEI
|`dec_IMEI_NAS_EPS_backtrack` |octetstring, IMEI |integer
|====================================================================================

= Terminology

== Abbreviations

NAS:: Non-Access-Stratum

EPS:: Evolved Packet System

TTCN-3:: Testing and Test Control Notation version 3

PDU:: Protocol Data Unit

= References

1. [[_1]]ETSI ES 201 873-1 v.4.1.2 (2009-07) +
The Testing and Test Control Notation version 3. Part 1: Core Language

2. [[_2]]109 21-1/CNL 113 862-1 Uen +
NAS EPS v15.2.0 Protocol Modules for TTCN-3 Toolset with TITAN, Product Revision Information

3. [[_3]] link:https://github.com/eclipse/titan.core/blob/master/usrguide/referenceguide[2/198 17-CRL 113 200/6] Uen +
Programmer’s Technical Reference for the TITAN TTCN-3 Test Executor

4. [[_4]] link:https://github.com/eclipse/titan.core/blob/master/usrguide/installationguide[1/1531-CRL 113 200/6] Uen +
Installation Guide for the TITAN TTCN-3 Test Executor

5. [[_5]]3GPP TS 24.301 V15.2.0 +
Technical Specification +
3rd Generation Partnership Project; +
Technical Specification Group Core Network and Terminals; +
Non-Access-Stratum (NAS) protocol for Evolved Packet System (EPS); Stage 3 +
(Release 15)

6. [[_6]]3GPP TS 24.008 V15.3.0 +
Technical Specification +
3rd Generation Partnership Project; +
Technical Specification Group Core Network and Terminals; +
Mobile radio interface Layer 3 specification; Core network protocols; Stage 3 +
(Release 15)

7. [[_7]]3GPP TS 24.011 V15.1.0 +
Technical Specification +
3rd Generation Partnership Project; +
Technical Specification Group Core Network and Terminals; +
Point-to-Point (PP) Short Message Service (SMS) support on mobile radio interface +
(Release 15)

8. [[_8]]3GPP TS 24.161 V15.0.0 +
Technical Specification +
3rd Generation Partnership Project; +
Technical Specification Group Core Network and Terminals; +
Network-Based IP Flow Mobility (NBIFOM); Stage 3 +
(Release 15)

= Change Information

== R1A

Initial implementation.

== R2A

Prepared to be open source. Behavior unchanged.
